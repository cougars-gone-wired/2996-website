# 2996 Website Code
The code for Team 2996's website, found at team2996.com

## Git Standards
Team 2996 adheres to strict git standards to help us produce higher quality code.
Please review and adhere to the git standards in the [git standards directory](./_git_documents).

